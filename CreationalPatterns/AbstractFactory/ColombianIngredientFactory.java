
public class ColombianIngredientFactory implements PizzaIngredientFactory {
 
	public Dough createDough() {
		return new ThinCrustDough();
	}
 
	public Sauce createSauce() {
		return new PlumTomatoSauce();
	}
 
	public Cheese createCheese() {
		return new MozzarellaCheese();
	}
 
	public Veggies[] createVeggies() {
		return null;
	}
 
	public Pepperoni createPepperoni() {
		return null;
	}

	public Clams createClam() {
		return null;
	}
}
