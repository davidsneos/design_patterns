
public class CriollaPizza extends Pizza {
	public CriollaPizza() {
		name = "Criolla Pizza";
		dough = "Colombian One";
		sauce = "Tomatico";
		toppings.add("Carnesita");
		toppings.add("Maiz");
	}
}
