
public class MiniDuckSimulator {
 
	public static void main(String[] args) {
 
		MallardDuck	mallard = new MallardDuck();
		RubberDuck rubberDuckie = new RubberDuck();
		
		mallard.performFly();
		mallard.performQuack();
	
		rubberDuckie.performFly();
		rubberDuckie.performQuack();
		
		rubberDuckie.setFlyBehavior(new FlyRocketPowered());
		
		rubberDuckie.performFly();
		rubberDuckie.performQuack();
	}
	
}
