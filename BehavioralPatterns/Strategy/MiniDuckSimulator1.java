
public class MiniDuckSimulator1 {
 
	public static void main(String[] args) {
 
		Duck myDuck = new RubberDuck();
		
		myDuck.performFly();
		myDuck.performQuack();

		myDuck.setQuackBehavior(new FakeQuack());
		
		myDuck.performQuack();
		myDuck.performQuack();
		myDuck.performQuack();
		
		myDuck.setQuackBehavior(new MuteQuack());

       	myDuck.performQuack();
		myDuck.performQuack();
		myDuck.performQuack();
	}
}
