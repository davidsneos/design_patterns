
public class LecheCondensada extends CondimentDecorator {
	Beverage beverage;
 
	public LecheCondensada(Beverage beverage) {
		this.beverage = beverage;
	}
 
	public String getDescription() {
		return beverage.getDescription() + ", LecheCondensada";
	}
 
	public double cost() {
		return .5 + beverage.cost();
	}
}
